package com.spajam2017.spajam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ViewSwitcher;
import android.text.format.Time;
import android.widget.CalendarView;
import android.widget.ImageView;

/**
 * Created by hyuma on 2017/06/17.
 */

public class Calendar extends AppCompatActivity{
    private String sweetDay;
    private ViewSwitcher viewSwitcher;
    private Time time = new Time("Asia/Tokyo");
    private CalendarView calendarView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calender);
        Intent intent = getIntent();

        // 甘い日を取得
        sweetDay = intent.getStringExtra("SweetDay");
        calendarView = (CalendarView)findViewById(R.id.calendarView);
        viewSwitcher = (ViewSwitcher)findViewById(R.id.viewSwitcher);
        initCalendar();
    }

    // 画面切り替え
    public void nextClick(View v) {
        viewSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.up));
        viewSwitcher.showNext();
    }

    // カレンダー日付を取得
    private void initCalendar(){
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String mYear = Integer.toString(year);
                String mMonth;
                String mDay;

                // 10以下のときの例外処理
                // day
                if ( dayOfMonth < 10 ){
                    mDay = new StringBuilder().append("0").append(dayOfMonth).toString();
                }else{
                    mDay = Integer.toString(dayOfMonth);
                }

                // month
                if ( (month + 1) < 10 ){
                    mMonth = new StringBuilder().append("0").append(month + 1).toString();
                }else{
                    mMonth = Integer.toString(month + 1);
                }

                String selectedDate = new StringBuilder().append(mYear)
                        .append(mMonth)
                        .append(mDay).toString();

                transitionView(selectedDate);
                System.out.println(selectedDate);
            }
        });
    }

    private void transitionView( String selectedDate ){
        // 遷移先に日付を送る
        Intent intent = new Intent(Calendar.this, Timeline.class);
        intent.putExtra("date",selectedDate);
        startActivity(intent);
    }
}
