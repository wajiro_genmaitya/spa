package com.spajam2017.spajam;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class UrlImage extends Activity {

    private ImageButton _button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _button = (ImageButton)findViewById(R.id.imageButton);
        _button.setImageResource(R.drawable.mon);
        _button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //イメージボタンが押された時の処理
                        Uri uri = Uri.parse("http://www.monteur.co.jp");
                        Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                        startActivity(intent);
                    }
                }
        );
    }
}
