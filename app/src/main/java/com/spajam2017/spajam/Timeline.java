package com.spajam2017.spajam;

/**
 * Created by hyuma on 2017/06/17.
 */
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Image;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Calendar;
import android.content.Intent;
import java.util.ArrayList;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import static android.R.attr.onClick;
import static com.spajam2017.spajam.R.id.hour;
import static com.spajam2017.spajam.R.id.textView;
import static java.lang.Math.abs;

/**
 * Created by KO on 2017/06/17.
 */

public class Timeline extends AppCompatActivity implements View.OnTouchListener {
    static final int ITEM_MAX = 3;
    static final int A_DAY = 24;

    //ここから
    //ここまで

    //ここから
    static final int TASK_MAX = 10;
    private String[] _taskName;
    private ImageView[] _imageView;
    private TextView[] _textView;
    //ここまで

    private String _date;
    private String[] _items;
    private int itemCount = 0;
    private ListView _listView;

    private ArrayList<ImageView> _arrayImg;

    private int preDx,preDy,newDx,newDy;
    //private TextView _textView;
    private TextView _setText;
    private ArrayList<String> _arrayList;
    private String[] _name;
    private String[] _task;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.timeline);

        _arrayList = new ArrayList<String>();
        _arrayImg = new ArrayList<ImageView>();

        //imageView = (ImageView) findViewById(R.id.taskImage);
        //imageView.setOnTouchListener(this);
        setIcon();

        //_textView = (TextView)findViewById(R.id.taskText);
        //_textView.setOnTouchListener(this);

        _setText = (TextView)findViewById(R.id.textView3);
        _setText.setOnTouchListener(this);

        _name = new String[ITEM_MAX];

        preDx = preDy = newDx = newDy = 0;

        _task = new String[A_DAY];
        for(int i = 0; i < A_DAY; i++) {
            _task[i] = "";

        }
        _date = intent.getStringExtra("date");
        setTitle(_date.substring(0,4) + "年" + _date.substring(4,6) + "月" + _date.substring(6,8) + "日");
        Calendar calendar = Calendar.getInstance();
        // 過去の時間は即実行されます
        calendar.set(Calendar.YEAR, Integer.parseInt(_date.substring(0,4)));
        calendar.set(Calendar.MONTH, Integer.parseInt(_date.substring(4,6)));
        calendar.set(Calendar.DATE, Integer.parseInt(_date.substring(6,8)));
        // calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.SECOND, 5);

        /*
        // 5秒後に設定
        //calendar.add(Calendar.,intent);
        Intent intent2 = new Intent(getApplicationContext(), AlarmBroadcastReceiver.class);

        intent2.putExtra("intentId", 2);
        PendingIntent pending = PendingIntent.getBroadcast(getApplicationContext(), 2, intent2, 0);

        // アラームをセットする
        AlarmManager am = (AlarmManager)this.getSystemService(ALARM_SERVICE);
        //
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pending);
        //トーストで設定されたことを表示
        */
        Toast.makeText(getApplicationContext(), "ALARMを設定しました", Toast.LENGTH_SHORT).show();
        String setTime = "設定時間："+Integer.parseInt(_date.substring(0,4))+"/"+Integer.parseInt(_date.substring(4,6))+"/"+(_date.substring(6,8))+" "+ intent2;
        _setText.setText(setTime);

        _listView = (ListView) findViewById(R.id.list);
        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String msg = position + "番目のアイテムがクリックされました";
                //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                _task[position] = "yo";
                //Toast.makeText(this, String.valueOf(inx), Toast.LENGTH_LONG).show();
                System.out.println("value: " + String.valueOf(position + _listView.getFirstVisiblePosition()));
                int first = _listView.getFirstVisiblePosition();
                int y = _listView.getChildAt(0).getTop();

                setList();
                _listView.setSelectionFromTop(first, y);

            }
        });
        setList();
    }

    private void setIcon(){
        //保存領域からタスクの種類をよみこんでいるよ
        SharedPreferences preferences = getSharedPreferences("TASK_ICON", Context.MODE_PRIVATE);
        int iconLen = preferences.getInt("iconLen", 0);
        if(iconLen == 0)Toast.makeText(this,"hozonsaretenaiyo", Toast.LENGTH_SHORT).show();
        _taskName = new String[iconLen];
        for(int i = 1; i <= iconLen; i++){
            _taskName[i - 1] = preferences.getString("task" + i, "");
        }

        //イメージビューを沢山つくっているよ
        _imageView = new ImageView[10];
        View im[] = {findViewById(R.id.img1), findViewById(R.id.img2), findViewById(R.id.img3),
                findViewById(R.id.img4), findViewById(R.id.img5), findViewById(R.id.img6),
                findViewById(R.id.img7), findViewById(R.id.img8), findViewById(R.id.img9), findViewById(R.id.img10)};

        _textView = new TextView[3];
        View tx[] = {findViewById(R.id.taskText1),findViewById(R.id.taskText2),findViewById(R.id.taskText3),
                findViewById(R.id.taskText4),findViewById(R.id.taskText5),findViewById(R.id.taskText6),
                findViewById(R.id.taskText7),findViewById(R.id.taskText8),findViewById(R.id.taskText9),findViewById(R.id.taskText10)

        };


        //タスクの種類分だけイメージビューを作っているよ
        for(int i = 0; i < _taskName.length; i++) {
            _imageView[i] = (ImageView) im[i];
            _imageView[i].setOnTouchListener(this);
            _imageView[i].setImageResource(R.drawable.apple);

            _textView[i] = (TextView) tx[i];
            _textView[i].setText(_taskName[i]);
        }

    }

    void saveTaskKind(){
        SharedPreferences prefs = getSharedPreferences("taskIcon", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("iconLen", _taskName.length);
        editor.putString("", "value");

        editor.apply();
    }



    //要らなくなるかも（かなしい）
    public int getView(int y) {
        //画面に映る一番上のList
        int firstVisiblePosition = _listView.getFirstVisiblePosition();
        //画面に見える分の数
        int childCount = _listView.getChildCount();

        int inx = -1;

        //画面上の一番上から順番に座標を取ってタッチした座標と比べてる
        for (int i = 0; i < childCount; i++) {
            int[] location = new int[2];
            _listView.getChildAt( i ).getLocationOnScreen(location);

            if(y < location[1]) break;
            inx = i;
        }

        //画面上の座標＋画面上の一番上の実Index
        return inx + firstVisiblePosition;
    }



    private void setList(){
        // ListView を取得
        ListView listView = _listView;

        ArrayList<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
        SimpleAdapter adapter;

        // SimpleAdapterに渡すArrayList作成
        list = createData(list);

        // リストビューに渡すアダプタを生成
        adapter = new SimpleAdapter(
                this,
                list,//ArrayList
                R.layout.timeline_list, //ListView内の1項目を定義したxml
                new String[] { "time","img","tex" },//mapのキー
                new int[] {R.id.hour, R.id.img, R.id.taskText });//item.xml内のid

        // アダプタをセット
        listView.setAdapter(adapter);
    }


    private ArrayList<Map<String,Object>> createData(ArrayList<Map<String,Object>> list){
        _items = new String[A_DAY];  //マジックナンバーを極力なくして
        for(int i = 0; i < A_DAY; i++){
            if(i < 10) _items[i] = "0" + i + ":00";
            _items[i]  =  i + ":00";
        }
        for (int n = 0; n < _items.length; n++) {
            Map data = new HashMap();
            data.put("time", _items[n]);
            if(!_task[n].equals("")) {
                data.put("img", R.drawable.apple);
                data.put("tex", _textView[1].getText().toString());
            }else{
                data.put("img", null);
                data.put("tex", null);
            }
            list.add(data);
        }
        return list;
    }



    //メニューを作成
    public boolean onCreateOptionsMenu(Menu menu){
        //メニューの名前
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }



    // タッチアクション
    public boolean onTouch(View v, MotionEvent event){
        /*

        // x,y 位置取得
        newDx = (int)event.getRawX();
        newDy = (int)event.getRawY();

        switch (event.getAction()) {

            // タッチダウンでdragされた
            case MotionEvent.ACTION_MOVE:

                // ACTION_MOVEでの位置
                int dx = v.getLeft() + (newDx - preDx);
                int dy = v.getTop() + (newDy - preDy);

                // ACTION_MOVEでの位置
                int tx = _textView.getLeft() + (newDx - preDx);
                int ty = _textView.getTop() + (newDy - preDy);

                // 画像の位置を設定する
                v.layout(dx, dy, dx + v.getWidth(), dy + v.getHeight());
                _textView.layout(tx, ty, tx + _textView.getWidth(), ty + _textView.getHeight());
                break;




            case MotionEvent.ACTION_UP:
                int inx = getView((int) event.getRawY());

                if(inx != -1)_task[inx] = "yo";
                //Toast.makeText(this, String.valueOf(inx), Toast.LENGTH_LONG).show();
                System.out.println("value: " + String.valueOf(inx + _listView.getFirstVisiblePosition()));
                int position = _listView.getFirstVisiblePosition();
                int y = _listView.getChildAt(0).getTop();

                setList();
                _listView.setSelectionFromTop(position, y);
                return true;



        }

        // タッチした位置を古い位置とする
        preDx = newDx;
        preDy = newDy;

        return true;

        */
        return false;
    }


    //
    public boolean onOptionsItemSelected(MenuItem item){
        //それぞれのメニュー
        int id = item.getItemId();
        //それぞれのメニューでどれが押されたかまた、その処理
        if(id == R.id.action_create){
            //ポップの作成
            create();

            return true;
        }else if(id == R.id.action_delete){
            delete();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    //
    private void create(){

        for( int i = 0; i < _name.length; i++ ) {
            final EditText editView = new EditText(this);
            new AlertDialog.Builder(this)
                    .setTitle("作成するタスクを入力してください")
                    //
                    .setView(editView)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //getTextでとってきたセーブ名をストリング型に変換して_savetitleへ代入
                            for (int i = 0; i < _name.length; i++) {
                                _name[i] = editView.getText().toString();
                                _arrayList.add(_name[i]);
                            }
                            //createTask();
                        }
                    })
                    .setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    })
                    .show();
            _arrayList.add(_name[i]);
        }

    }

    private void delete(){
        new AlertDialog.Builder(this)
                .setTitle("タスクを削除しますか？")
                //
                .setPositiveButton("OK",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton){
                        //文字と画像の削除をおこなう(とりあえず０番目を削除)
                        //imageView.setVisibility(ImageView.INVISIBLE );
                        //_textView.setText("");
                    }
                })
                .setNegativeButton("キャンセル", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton){
                    }
                })
                .show();

        /*//これまで保存してきたタスクのタイトルリスト
        final String[] saveTask = this.fileList();
        AlertDialog.Builder listDlg = new AlertDialog.Builder(this);
        listDlg.setTitle("削除するタスクの選択");
        listDlg.setItems(
                saveTask,
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        //開きたいタスクの番号がwhichに入る
                        deleteFile(saveTask[which]);
                    }
                });
        //画面に出力
        listDlg.create().show();*/
    }
    // アラート処理
    public void onReceive(Context context, Intent intent) {
        // intentID (requestCode) を取り出す
        int bid = intent.getIntExtra("intentId",0);
        // RecieverからMainActivityを起動させる
        Intent intent2 = new Intent(context, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, bid, intent2, 0);

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.apple)
                .setTicker("時間です")
                .setWhen(System.currentTimeMillis())
                .setContentTitle("TestAlarm")
                .setContentText("時間になりました")
                // 音、バイブレート、LEDで通知
                .setDefaults(Notification.DEFAULT_ALL)
                // 通知をタップした時にMainActivityを立ち上げる
                .setContentIntent(pendingIntent)
                .build();

        // 古い通知を削除
        notificationManager.cancelAll();
        // 通知
        notificationManager.notify(R.string.app_name, notification);
    }
}