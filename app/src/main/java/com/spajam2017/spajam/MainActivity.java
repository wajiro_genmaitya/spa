package com.spajam2017.spajam;

import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.view.View;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Build;
import android.annotation.TargetApi;
import android.widget.CalendarView;
import android.widget.TextView;

import android.os.Bundle;

import android.widget.Toast;
import android.widget.ImageView;
import android.view.Gravity;
import android.view.animation.AlphaAnimation;
import android.text.format.Time;
import android.content.Intent;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private String sweetDay;
    private ViewSwitcher viewSwitcher;
    private Time time = new Time("Asia/Tokyo");
    private CalendarView calendarView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button button = (Button) findViewById(R.id.ScheduleIcon);
        setTitle("パインあめ");

        setContentView(R.layout.calender);
        Intent intent = getIntent();

        makeSaveTaskIcon();
        // 甘い日を取得
        //sweetDay = intent.getStringExtra("SweetDay");
        calendarView = (CalendarView)findViewById(R.id.calendarView);
        viewSwitcher = (ViewSwitcher)findViewById(R.id.viewSwitcher);
        initCalendar();

        /*

        // ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                flagmentManager = getFragmentManager();
                sweetDay = String.valueOf((random.nextInt(30) + 1));
                String value = new StringBuilder().append("今月の甘い日は").append(sweetDay).append("日です").toString();
                Bundle args = new Bundle();
                args.putString("sweetDay", sweetDay); //引数
                dialogFragment = new AlertDialogFragment();
                dialogFragment.setArguments(args);
                dialogFragment.show(flagmentManager, "dialog");
            }
        });
        */
    }

    // 画面切り替え
    public void nextClick(View v) {
        viewSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.up));
        viewSwitcher.showNext();
    }

    // カレンダー日付を取得
    private void initCalendar(){
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String mYear = Integer.toString(year);
                String mMonth;
                String mDay;

                // 10以下のときの例外処理
                // day
                if ( dayOfMonth < 10 ){
                    mDay = new StringBuilder().append("0").append(dayOfMonth).toString();
                }else{
                    mDay = Integer.toString(dayOfMonth);
                }

                // month
                if ( (month + 1) < 10 ){
                    mMonth = new StringBuilder().append("0").append(month + 1).toString();
                }else{
                    mMonth = Integer.toString(month + 1);
                }

                String selectedDate = new StringBuilder().append(mYear)
                        .append(mMonth)
                        .append(mDay).toString();

                transitionView(selectedDate);
                System.out.println(selectedDate);
            }
        });
    }

    private void transitionView( String selectedDate ){
        // 遷移先に日付を送る
        Intent intent = new Intent(MainActivity.this, Timeline.class);
        intent.putExtra("date",selectedDate);
        startActivity(intent);
    }

//タスクアイコンの初期設定を作成
    private void makeSaveTaskIcon(){
        SharedPreferences preferences = getSharedPreferences("TASK_ICON", Context.MODE_PRIVATE);
        int iconLen = preferences.getInt("iconLen", 0);

        if(iconLen == 0){
            Toast.makeText(this,"初期設定", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("iconLen",3);
            editor.putString("task1", "仕事");
            editor.putString("task2", "メッセージ");
            editor.putString("task3", "遊び");
            editor.apply();

        }
    }

    /*
    public static class AlertDialogFragment extends DialogFragment {
        private AlertDialog dialog ;
        private AlertDialog.Builder alert;

        // target を　指定
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            alert = new AlertDialog.Builder(getActivity());
            final String sweetDay = getArguments().getString("sweetDay");  // 甘い時間を取得
            alert.setTitle("今月の甘い時間は" + sweetDay + "日です");

            // カスタムレイアウトの生成
            View alertView = getActivity().getLayoutInflater().inflate(R.layout.alert_lyout, null);

            // alert_layout.xmlにあるボタンIDを使う

            // imageを設定
            ImageView image = (ImageView) alertView.findViewById(R.id.bag_1);
            int day = Integer.parseInt(sweetDay);
            if(day > 20){
                image.setImageResource(R.drawable.omikuji_daikichi);
            }else if( day > 10 ){
                image.setImageResource(R.drawable.omikuji_chuukichi);
            }else{
                image.setImageResource(R.drawable.syoukichi);
            }

            image.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    Intent intent = new Intent((MainActivity) getActivity(), Calendar.class);
                    intent.putExtra("SweetDay",Integer.parseInt(sweetDay));
                    startActivity(intent);
                    // Dialogを消す
                    getDialog().dismiss();
                }
            });

            // ViewをAlertDialog.Builderに追加
            alert.setView(alertView);

            // Dialogを生成
            dialog = alert.create();
            dialog.show();

            return dialog;
        }
    }
    */
}
