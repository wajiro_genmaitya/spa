package com.spajam2017.spajam;

/**
 * Created by KO on 2017/06/18.
 */

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by amyu on 15/02/28.
 */
public class HitRectListView extends ListView {

    public HitRectListView(Context context) {
        super(context);
    }

    public HitRectListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HitRectListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HitRectListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public int getTouchChildIndex(final int x, final int y) {
        int childCount = getChildCount();
        Rect rect = new Rect();

        for (int index = 0; index < childCount; index++) {
            getChildAt(index).getHitRect(rect);
            if (rect.contains(x, y)) {
                return index;
            }
        }
        return -1;
    }
}